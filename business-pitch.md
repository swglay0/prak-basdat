# 1 Penggunaan Subquery pada produk digital 
# 2 Penggunaan Transaction pada produk digital 
# 3 Menampilkan Use Case Table untuk produk digital
# 4 Web Service (CRUD) dari produk digital
CRUD Postman : 
![](https://gitlab.com/swglay0/prak-basdat/-/raw/main/CRUD_POSTMAN.gif)
Code : 
![](https://gitlab.com/swglay0/prak-basdat/-/raw/main/CRUD_.png)
# 5 Visualisasi data untuk business intelligence produk digital
Visualisasi Data Order pada bulan Juli : 
![](https://gitlab.com/swglay0/prak-basdat/-/raw/main/visdat.png)
# 6 Built-in funtion dengan ketentuan
Regex : 
![](https://gitlab.com/swglay0/prak-basdat/-/raw/main/regex.png)
# 7 Penggunaan Procedure / Function dan Trigger pada produk digital
# 8 Demonstrasi Data Control Language (DCL) pada produk digital
# 9 Constraint yang digunakan pada produk digital
![](https://gitlab.com/swglay0/prak-basdat/-/raw/main/FK.png)
# 10 Demonstrasi produk digital (Youtube)
# 11 Demonstrasi UI untuk CRUDnya
![](https://gitlab.com/swglay0/prak-basdat/-/raw/main/UI.gif)

