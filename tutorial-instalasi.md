# No 1
Instalasi PostgreSQL
1. Download postgreSQL di website official nya https://www.enterprisedb.com/downloads/postgres-postgresql-downloads , jika sudah didownload buka file di folder lalu jalankan untuk memulai instalasi

![](https://gitlab.com/swglay0/prak-basdat/-/raw/main/Instalasi%20no1/1.PNG)

2. Seperti ini tampilan ketika file pertama dibuka langsung next saja 

![](https://gitlab.com/swglay0/prak-basdat/-/raw/main/Instalasi%20no1/2.PNG)

3. Pilih dimana file akan disimpan lalu klik next

![](https://gitlab.com/swglay0/prak-basdat/-/raw/main/Instalasi%20no1/3.PNG)

4. Pilih komponen yang akan diinstall, disini saya menginstall semua lalu klik next

![](https://gitlab.com/swglay0/prak-basdat/-/raw/main/Instalasi%20no1/4.PNG)

5. Pilih dimana file akan di store

![](https://gitlab.com/swglay0/prak-basdat/-/raw/main/Instalasi%20no1/5.PNG)

6. Tentukan port server yang digunakan, port bebas tidak harus sesuai dengan defaultnya

![](https://gitlab.com/swglay0/prak-basdat/-/raw/main/Instalasi%20no1/6.PNG)

7. Disini pilih default saja

![](https://gitlab.com/swglay0/prak-basdat/-/raw/main/Instalasi%20no1/7.PNG)

8. Sebelum memulai proses instalasi akan muncul Pre Installation Summary yang menunjukan informasi tentang instalasi yang akan dijalankan, jika sudah sesuai dengan yang kita inginkan klik next

![](https://gitlab.com/swglay0/prak-basdat/-/raw/main/Instalasi%20no1/8.PNG)

9. klik next saja untuk memulai proses install

![](https://gitlab.com/swglay0/prak-basdat/-/raw/main/Instalasi%20no1/9.PNG)

10. Proses Instalasi sudah dimulai, tunggu hingga selesai

![](https://gitlab.com/swglay0/prak-basdat/-/raw/main/Instalasi%20no1/10.PNG)

11. PostgreSQL sudah selesai terinstall, sebelum klik Finish uncheck "Launch Stack Builder at exit".

![](https://gitlab.com/swglay0/prak-basdat/-/raw/main/Instalasi%20no1/11.PNG)

# No 2
Konfigurasi

1. Buka pgadmin yang sudah diinstall
![](https://gitlab.com/swglay0/prak-basdat/-/raw/main/Instalasi%20no1/12.PNG)

2. Masukkan password sesuai dengan yang tadi kita set pada proses instalasi

![](https://gitlab.com/swglay0/prak-basdat/-/raw/main/Instalasi%20no1/13.PNG)

3. Buka server postgres lalu masukkan password

![](https://gitlab.com/swglay0/prak-basdat/-/raw/main/Instalasi%20no1/14.PNG)

4. Maka tampilan seperti ini 

![](https://gitlab.com/swglay0/prak-basdat/-/raw/main/Instalasi%20no1/15.PNG)

5. Buka DBeaver lalu klik yang bergambar seperti colokan di kiri atas

![](https://gitlab.com/swglay0/prak-basdat/-/raw/main/Instalasi%20no1/16.PNG)

6. Disini pilih database PostgreSQL

![](https://gitlab.com/swglay0/prak-basdat/-/raw/main/Instalasi%20no1/17.PNG)

7. isi username, password, dan juga port sesuai dengan yang kita set tadi

![](https://gitlab.com/swglay0/prak-basdat/-/raw/main/Instalasi%20no1/18.PNG)

8. Klik Test Connection dulu untuk cek koneksinya

![](https://gitlab.com/swglay0/prak-basdat/-/raw/main/Instalasi%20no1/20.PNG)

# No 3
Connect 
![](https://gitlab.com/swglay0/prak-basdat/-/raw/main/Instalasi%20no1/21.PNG)
